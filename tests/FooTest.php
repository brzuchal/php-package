<?php declare(strict_types=1);

namespace MyVendor\MyLibrary\Tests;

use MyVendor\MyLibrary\Foo;
use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    public function test(): void
    {
        $foo = new Foo();
        $this->assertInstanceOf(Foo::class, $foo);
    }
}
