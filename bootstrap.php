<?php

class PackageParser
{
    private static $autoloaders = [];

    public static function registerAutoloader(string $name, callable $autoloader): void
    {
        self::$autoloaders[$name] = $autoloader;
    }

    public static function load(string $filename): void
    {
        $directory = dirname($filename);
        $callbacks = [];
        foreach (self::$autoloaders as $name => $autoloader) {
            $callbacks["!{$name}"] = static function ($value) use ($autoloader, $directory): callable {
                return self::createAutoloaderBuilder($autoloader, $directory, $value);
            };
        }
        $docs = yaml_parse(\file_get_contents($filename), -1, $ndocs, $callbacks);
        foreach ($docs as $package) {
            [
//                'version' => $phpVersion,
                'name' => $packageName,
                'autoload' => [
                    'classes' => $builders,
                ],
//                'declare' => $declares,
            ] = $package;
            self::createClassesAutoloaders($builders, $packageName);
        }
    }

    public static function createAutoloaderBuilder(callable $autoloader, string $directory, $value): callable
    {
        return static function (string $packageName) use ($autoloader, $directory, $value): void {
            $autoloader($value, $packageName, $directory);
        };
    }

    private static function createClassesAutoloaders(array $definitions, string $packageName): array
    {
        $classesAutoload = [];
        foreach ($definitions as $definition) {
            if (is_callable($definition)) {
                $classesAutoload[] = $definition($packageName);
            }
        }
        return $classesAutoload;
    }
}

PackageParser::registerAutoloader('psr4', static function ($value, string $packageName, string $directory) {
    if (is_string($value)) {
        $path = $directory . '/' . $value;
        spl_autoload_register(static function (string $className) use ($packageName, $path): void {
            $filename = $path . str_replace([$packageName, '\\'], ['', '/'], $className) . '.php';
            if (!file_exists($filename)) return;
            @require_once $filename;
        });
    } elseif (is_array($value)) {
        foreach ($value as $prefix => $path) {
            $path = $directory . '/' . $path;
            spl_autoload_register(static function (string $className) use ($packageName, $path): void {
                $filename = $path . str_replace([$packageName, '\\'], ['', '/'], $className) . '.php';
                if (!file_exists($filename)) return;
                @require_once $filename;
            });
        }
    }
});
PackageParser::load(__DIR__ . '/package.yaml');
