<?php

require_once __DIR__ . '/bootstrap.php';

$foo = new \MyVendor\MyLibrary\Foo();
var_dump($foo);

$baz = new \Foo\Bar\Bar();
var_dump($baz);
